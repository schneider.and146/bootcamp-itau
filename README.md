# Vamos fazer nosso primeiro deploy com o GitLab CI?

### Siga estes passos
1. Crie seu Runner *self-hosted* do GitLab CI;
2. Crie uma pipeline com no arquivo .gitlab-ci.yml que, ao perceber uma mudança na branch *main*, execute dois estágios:
   - Step 1: Listar os arquivos do repositório;
   - Step 2: Instalar o pacote *curl* e depois fazer o upload do arquivo **index.html** via *CURL* no endpoint do nosso servidor:
```sh
# como instalar o pacote cURL
$ apt-get update && apt-get -y install curl

# modelagem da chamada a API
Método: PUT
URL: https://ktxdfuuszshdwe2fpi6niua45e0pduww.lambda-url.us-east-1.on.aws/
Cabeçalhos obrigatórios:
  myToken: BNUhVeITc3kgQM4g07rat62XKmiMYf
  myPath: <verificar seu path no arquivo paths-permitidos.txt>

# exemplo da chamada
$ curl -H 'authToken: BNUhVeITc3kgQM4g07rat62XKmiMYf' -H 'myPath: <seu-path>' -T index.html https://ktxdfuuszshdwe2fpi6niua45e0pduww.lambda-url.us-east-1.on.aws/
 ```
**Observações importantes**:
1. Tudo deverá ser executado pelo seu runner **self-hosted**; 
2. Os steps deverão pertencer ao mesmo estágio e ser sequenciais, ou seja, primeiro é executando o step 1 e se
ele rodar com sucesso, é executado o step 2;
3. Para todos os steps, pode ser usada a imagem **ubuntu:latest**;

Se tudo for feito corretamente, ao acessar a URL `http://bootcamp-itau.s3-website-us-east-1.amazonaws.com/<seu-path>/index.html` no seu browser, você verá uma página web indicando o sucesso do seu deploy.

